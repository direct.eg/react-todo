import Todo from "./components/Todo";

function App(props) {

  const taskList = props.tasks?.map( (task) => <Todo 
    name={task.name} 
    status={task.status} 
    id={task.id}  />   
    ) ;

  return (
    <div className="todoapp stack-large">
      <h1>What needs to be done?</h1>

      <form>
        <input type="text" name="task-name" className="input input_lg" />
        <button type="submit" className='btn btn_primary btn_lg' >Add</button>

      </form>

      <div className="filters btn-group" >
        <button type="button" className='btn toggle-btn' >All</button>
        <button type="button" className='btn toggle-btn' >Active</button>
        <button type="button" className='btn toggle-btn' >Completed</button>
      </div>

      <h2>3 tasks remaining</h2>

      <ul className="todo-list stack-large stack-exception" >
        
        {taskList}

      </ul>

    </div>
  );
}

export default App;
